package org.cef.browser;

import org.cef.DummyComponent;
import org.cef.callback.CefDragData;
import org.cef.handler.CefClientHandler;
import org.cef.handler.CefRenderHandler;
import org.lwjgl.BufferUtils;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.nio.ByteBuffer;

/**
 * This is a custom implementation of the {@link CefBrowser} interface. It is placed into the foreign package org.cef.browser because of the
 * unfortunate decision of the JCEF developers to hide most parts of their API. Since JCEF uses JOGL and an {@link
 * com.jogamp.opengl.swt.GLCanvas} the implementation is not sufficient to be used in a raw OpenGL environment, especially within the LWJGL
 * framework.
 *
 * The implementation is a modified version of Montoyo's CefBrowserOsr from github (see the link below).
 *
 * It adapts the behavior of the original {@link CefBrowserOsr} implementation but replaces the {@link com.jogamp.opengl.swt.GLCanvas} with
 * a raw texture that is drawn onto the active framebuffer eventually. See {@link CefRendererLwjgl}
 *
 * @see <a href="https://github.com/montoyo/mcef/blob/master/src/main/java/org/cef/browser/CefBrowserOsr.java">Montoyo's CefBrowserOsr</a>
 */
public class CefBrowserOsrLwjgl extends CefBrowser_N implements CefRenderHandler {
    private static final boolean CLEANUP = true;
    private final CefRendererLwjgl renderer;
    private final Rectangle browserRect = new Rectangle(0, 0, 1, 1);  // Work around CEF issue #1437. (original JCEF code)
    private final CefClientHandler clientHandler;
    private final String currentUrl;
    private final boolean isTransparent;
    private final CefRequestContext context;
    private final DummyComponent dummy = new DummyComponent();
    private CefBrowserOsrLwjgl parent = null;
    private CefBrowserOsrLwjgl devTools = null;

    public CefBrowserOsrLwjgl(CefClientHandler clientHandler,
            String url,
            boolean transparent,
            CefRequestContext context) {
        this(clientHandler, url, transparent, context, null, null);
    }

    private CefBrowserOsrLwjgl(CefClientHandler clientHandler,
            String url,
            boolean transparent,
            CefRequestContext context,
            CefBrowserOsrLwjgl parent,
            Point inspectAt) {
        super();
        isTransparent = transparent;
        renderer = new CefRendererLwjgl(transparent);
        this.clientHandler = clientHandler;
        currentUrl = url;
        this.context = context;
        this.parent = parent;
        createGLCanvas();
    }

    public int getTextureID() {
        return renderer.textureIdArray[0];
    }

    @Override
    public Component getUIComponent() {
        return dummy;
    }

    @Override
    public CefRenderHandler getRenderHandler() {
        return this;
    }

    @Override
    public synchronized void close() {
        if (context != null) { context.dispose(); }
        if (parent != null) {
            parent.closeDevTools();
            parent.devTools = null;
            parent = null;
        }

        if (CLEANUP) {
            renderer.cleanup();
        }

        super.close();
    }

    @Override
    public synchronized CefBrowser getDevTools() {
        return getDevTools(null);
    }

    @Override
    public synchronized CefBrowser getDevTools(Point inspectAt) {
        if (devTools == null) {
            devTools = new CefBrowserOsrLwjgl(clientHandler,
                                              currentUrl,
                                              isTransparent,
                                              context,
                                              this,
                                              inspectAt
            );
        }
        return devTools;
    }

    public void resize(int width, int height) {
        browserRect.setBounds(0, 0, width, height);
        dummy.setBounds(browserRect);
        dummy.setVisible(true);
        wasResized(width, height);
    }

    public void draw(double x1, double y1, double x2, double y2) {
        renderer.render(x1, y1, x2, y2);
    }

    @SuppressWarnings("serial")
    private void createGLCanvas() {
        createBrowser(clientHandler, 0, currentUrl, isTransparent, null, context);
    }

    @Override
    public Rectangle getViewRect(CefBrowser browser) {
        return browserRect;
    }

    @Override
    public Point getScreenPoint(CefBrowser browser, Point viewPoint) {
        return viewPoint;
    }

    @Override
    public void onPopupShow(CefBrowser browser, boolean show) {
        if (!show) {
            renderer.clearPopupRects();
            invalidate();
        }
    }

    private static class PaintData {
        private ByteBuffer buffer;
        private int width;
        private int height;
        private Rectangle[] dirtyRects;
        private boolean hasFrame;
        private boolean fullReRender;
    }

    private final PaintData paintData = new PaintData();

    @Override
    public void onPaint(CefBrowser browser, boolean popup, Rectangle[] dirtyRects, ByteBuffer buffer, int width, int height) {
        if (popup) { return; }
        final int size = (width * height) << 2;

        synchronized (paintData) {
            if (buffer.limit() > size) {
                System.out.println("Skipping MCEF browser frame, data is too heavy"); //TODO: Don't spam
            } else {
                if (paintData.hasFrame) //The previous frame was not uploaded to GL texture, so we skip it and render this on instead
                { paintData.fullReRender = true; }

                if (paintData.buffer == null || size != paintData.buffer.capacity()) //This only happens when the browser gets resized
                { paintData.buffer = BufferUtils.createByteBuffer(size); }

                paintData.buffer.position(0);
                paintData.buffer.limit(buffer.limit());
                buffer.position(0);
                paintData.buffer.put(buffer);
                paintData.buffer.position(0);

                paintData.width = width;
                paintData.height = height;
                paintData.dirtyRects = dirtyRects;
                paintData.hasFrame = true;
            }
        }
    }

    public void mcefUpdate() {
        synchronized (paintData) {
            if (paintData.hasFrame) {
                renderer.onPaint(false, paintData.dirtyRects, paintData.buffer, paintData.width, paintData.height, true);
                paintData.hasFrame = false;
                paintData.fullReRender = false;
            }
        }
    }

    @Override
    public void onPopupSize(CefBrowser browser, Rectangle size) {
        renderer.onPopupSize(size);
    }

    public void injectMouseMove(int x, int y, int mods, boolean pressed) {
        MouseEvent ev = new MouseEvent(dummy, pressed ? MouseEvent.MOUSE_DRAGGED : MouseEvent.MOUSE_MOVED, 0, mods, x, y, 0, false);
        sendMouseEvent(ev);
    }

    public void injectMouseButton(int x, int y, int mods, int btn, boolean pressed, int ccnt) {
        MouseEvent ev =
                new MouseEvent(dummy, pressed ? MouseEvent.MOUSE_PRESSED : MouseEvent.MOUSE_RELEASED, 0, mods, x, y, ccnt, false, btn);
        sendMouseEvent(ev);
    }

    public void injectKeyTyped(char c, int mods) {
        KeyEvent ev = new KeyEvent(dummy, KeyEvent.KEY_TYPED, 0, mods, 0, c);
        sendKeyEvent(ev);
    }

    public void injectKeyPressed(char c, int mods) {
        KeyEvent ev = new KeyEvent(dummy, KeyEvent.KEY_PRESSED, 0, mods, 0, c);
        sendKeyEvent(ev);
    }

    public void injectKeyReleased(char c, int mods) {
        KeyEvent ev = new KeyEvent(dummy, KeyEvent.KEY_RELEASED, 0, mods, 0, c);
        sendKeyEvent(ev);
    }

    public void injectMouseWheel(int x, int y, int mods, int amount, int rot) {
        MouseWheelEvent ev =
                new MouseWheelEvent(dummy, MouseEvent.MOUSE_WHEEL, 0, mods, x, y, 0, false, MouseWheelEvent.WHEEL_UNIT_SCROLL, amount, rot);
        sendMouseWheelEvent(ev);
    }

    public void onCursorChange(CefBrowser browser, int cursorType) {
    }

    public boolean startDragging(CefBrowser browser,
            CefDragData dragData,
            int mask,
            int x,
            int y) {
        // TODO(JCEF) Prepared for DnD support using OSR mode.
        return false;
    }

    public void updateDragCursor(CefBrowser browser, int operation) {
        // TODO(JCEF) Prepared for DnD support using OSR mode.
    }

    public void runJS(String script, String frame) {
        executeJavaScript(script, frame, 0);
    }

    public boolean isPageLoading() {
        return isLoading();
    }
}
