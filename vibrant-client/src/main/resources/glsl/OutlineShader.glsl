//This shader is used to outline objects with fading
#version 120

//The sampler used to sample from when taking samples from the texture
uniform sampler2D diffuseSampler;

//The intensity which is the alpha value of the faded fragment
uniform float fadeIntensity;

//The radius of which the samples should be taken from
uniform int sampleRadius;

//The color of the outline
uniform vec3 outlineColor;

//The size of a texel on the texture
uniform vec2 texelSize;

void main(void) {
    //Sample actual color this fragment shader called for
    vec4 color = texture2D(diffuseSampler, gl_TexCoord[0].xy);

    //Fragment has alpha not zero and therefore is an object to outline but not a fragment of our interest
    if (color.a != 0) {
        //Make object fragment transparent
        gl_FragColor = vec4(0, 0, 0, 0);
    } else {
        //Value that defines the average alpha of the result fragment color
        float alpha = 0;
        float sampleRadiusSq = float(sampleRadius * sampleRadius);

        //Sample all fragments in sample radius
        for (int x = -sampleRadius; x <= sampleRadius; x++) {
            for (int y = -sampleRadius; y <= sampleRadius; y++) {
                //Sample color with sample x and y added
                vec4 sampleColor = texture2D(diffuseSampler, gl_TexCoord[0].xy + vec2(x * texelSize.x, y * texelSize.y));

                //When a fragment already has alpha then distance can be measured and added to alpha average
                if (sampleColor.a != 0) {
                    //Adds inverted distance to alpha for fading more distant fragments
                    alpha += clamp(sampleRadiusSq - (x * x + y * y), 0, sampleRadiusSq) * fadeIntensity;
                }
            }
        }

        //Sets fragment color with alpha divided by fade intensity
        gl_FragColor = vec4(outlineColor, alpha);
    }
}