package net.cydhra.vibrant.modules.misc

import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.settings.by
import net.cydhra.vibrant.settings.clamped
import net.cydhra.vibrant.settings.setting
import org.lwjgl.input.Keyboard

class TimerModule : Module(name = "Timer", initialKeycode = Keyboard.KEY_U) {

    val speed by setting(this, "Speed", 2f) {
        clamped(1f, 10f)
        increment by 0.1f
        decrement by -0.1f
    }

    override fun onEnable() {
	mc.timer.timerSpeed = speed
    }

    override fun onDisable() {
        mc.timer.timerSpeed = 1f
    }

}
