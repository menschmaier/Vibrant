package net.cydhra.vibrant.modules.movement

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.world.blocks.VibrantBlockStairs
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.settings.by
import net.cydhra.vibrant.settings.clamped
import net.cydhra.vibrant.settings.setting

/**
 * A module that speeds up on ladders, stairs, etc
 */
class TerrainSpeedModule : Module("TerrainSpeed", DefaultCategories.MOVEMENT) {

    val ladder by setting(this, "Ladder", true)
    val stairs by setting(this, "Stairs", true)
    val ladderSpeed by setting(this, "LadderSpeed", 2.5) {
        clamped(1.0, 5.0)
        increment by 0.1
        decrement by -0.1
    }

    val stairsSpeed by setting(this, "StairsSpeed", 1.5) {
        clamped(1.0, 5.0)
        increment by 0.1
        decrement by -0.1
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer != null) {
            if (ladder) {
                if (mc.thePlayer!!.isCollidedHorizontally && mc.thePlayer!!.isClimbing()) {
                    mc.thePlayer!!.motionY *= ladderSpeed
                }
            }
            if (stairs) {
                val playerStandingPosition =
                        factory.newBlockPosition(mc.thePlayer!!.posX.toInt(), mc.thePlayer!!.posY.toInt() - 1, mc.thePlayer!!.posZ.toInt())
                if (mc.theWorld!!.getBlockInfoAt(playerStandingPosition) is VibrantBlockStairs) {
                    if(mc.thePlayer!!.onGround) {
                        mc.thePlayer!!.motionX *= stairsSpeed
                        mc.thePlayer!!.motionZ *= stairsSpeed
                    }
                }
            }
        }
    }
}