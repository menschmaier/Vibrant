package net.cydhra.vibrant.modules.gui

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.api.render.VibrantScaledResolution
import net.cydhra.vibrant.events.render.RenderOverlayEvent
import net.cydhra.vibrant.gui.util.fontrenderer.FontRenderers
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.modulesystem.ModuleManager
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.*
import java.awt.Color
import java.awt.Font

class HudModule : Module("Hud", DefaultCategories.SYSTEM, Keyboard.KEY_H) {

    private val font = Font("Arial", Font.BOLD, 18)
    private val guiController = VibrantClient.factory.newGuiController()

    override fun initialize() {
        this.isEnabled = true
    }

    @EventHandler
    fun onRenderOverlay(e: RenderOverlayEvent) {
        drawActiveModules(e.scaledResolution)
        drawWatermarkIcon()
    }

    fun drawWatermarkIcon() {
        val scale = 0.1

        val width = 519F
        val height = 494F

        val borderOffset = 15

        val col = Color.getHSBColor(1F, 0F, 1F)

        val alpha = 40

        mc.glStateManager.pushMatrix()

        glPushAttrib(GL11.GL_ENABLE_BIT)

        glEnable(GL_BLEND)

        glScaled(scale, scale, 0.0)

        mc.glStateManager.color(Color(col.red, col.green, col.blue, alpha))

        mc.getTextureManagerInstance().bindTexture("textures/vibrant.png")
        guiController.drawRectWithCustomSizedTexture(borderOffset, borderOffset, 0F, 0F, width.toInt(), height.toInt(), width, height)

        glPopAttrib()

        mc.glStateManager.popMatrix()
    }

    fun drawActiveModules(scaledResolution: VibrantScaledResolution) {
        mc.glStateManager.disableTexture2D()

        val fontRenderer = FontRenderers[font]

        glLineWidth(1.0F)
        glBegin(GL11.GL_LINE_STRIP)
        var offset = 0.0
        for (module in ModuleManager.modules) {
            if (module.isEnabled && module.category != DefaultCategories.SYSTEM) {
                val incrementY = fontRenderer.fontHeight + 4F
                val x = scaledResolution.getScaledWidth() - fontRenderer.getStringWidth(module.displayName) - 2F
                val y = offset

                offset += incrementY

                glVertex2d(x - 1, y - 1)
                glVertex2d(x - 1, y + incrementY - 1)
            }
        }
        glVertex2d(scaledResolution.getScaledWidth().toDouble(), offset - 1)
        glEnd()

        mc.glStateManager.enableTexture2D()

        offset = 2.0
        for (module in ModuleManager.modules) {
            if (module.isEnabled && module.category != DefaultCategories.SYSTEM) {
                fontRenderer.drawStringWithShadow(module.displayName,
                        scaledResolution.getScaledWidth() - fontRenderer.getStringWidth(module.displayName) - 1F,
                        offset,
                        Color.WHITE
                )
                offset += fontRenderer.fontHeight + 3
            }
        }
    }
}
