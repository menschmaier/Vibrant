package net.cydhra.vibrant.modules.visual

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.entity.VibrantZombie
import net.cydhra.vibrant.api.render.VibrantRenderGlobal
import net.cydhra.vibrant.events.render.RenderOverlayEvent
import net.cydhra.vibrant.gui.util.RenderUtil
import net.cydhra.vibrant.gui.util.StencilUtil
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.settings.by
import net.cydhra.vibrant.settings.clamped
import net.cydhra.vibrant.settings.cycling
import net.cydhra.vibrant.settings.setting
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11
import java.awt.Color

const val ZOMBY_OFFSET = 40

/**
 *
 */
class MinimapModule : Module("Minimap", DefaultCategories.VISUAL, Keyboard.KEY_Z) {

    /**
     * The angle in which the map is tilted (90 degrees mean straight looking down onto the map's surface)
     */
    private val perspectiveAngle by setting(this, "Map Angle", 45) {
        clamped(30, 70)
        increment by 1
        decrement by -1
    }

    /**
     * Radius of the map in the HUD
     */
    private val mapRadius by setting(this, "Map Radius", 80) {
        clamped(50, 100)
        increment by 5
        decrement by -5
    }

    /**
     * Whether or not to draw a coordinate system
     */
    private val showLines by setting(this, "Show Map Axis", true)

    /**
     * Line width of the coordinate system
     */
    private val lineWidth by setting(this, "Axis Strength", 0.2) {
        clamped(0.1, 1.0)
        increment by 0.05
        decrement by -0.05
    }

    /**
     * Shape of the map in the HUD
     */
    private val mapShape by setting(this, "Shape", Shape.CIRCLE) {
        increment cycling Shape.values()
    }

    private val showBorder by setting(this, "Show Border", true)

    private val borderSize by setting(this, "Border Width", 2) {
        clamped(1, 10)
        increment by 1
        decrement by -1
    }

    private val borderColor by setting(this, "Border Color", Color.BLACK)

    /**
     * A helper entity used as view entity to properly adjust the frustum check
     */
    private var zombie: VibrantZombie? = null

    @EventHandler
    fun onRender2D(e: RenderOverlayEvent) {
        if (mc.thePlayer == null)
            return

        if (this.zombie == null)
            zombie = factory.createZombie(mc.theWorld!!)

        mc.entityRenderer.setupOverlayRendering()

        mc.glStateManager.pushAttrib()
        mc.glStateManager.pushMatrix()

        // translate origin into the mid of the map
        val sc = factory.newScaledResolution()
        GL11.glTranslatef((sc.getScaledWidth() - mapRadius).toFloat(), (sc.getScaledHeight() - mapRadius).toFloat(), 0f)

        // prepare 2D
        mc.glStateManager.disableDepth()
        mc.glStateManager.disableTexture2D()
        mc.glStateManager.enableBlend()
        mc.glStateManager.enableLineSmooth()

        // draw background that is the border
        drawMapShape(mapRadius + borderSize, borderColor)

        // prepare circular stencil
        StencilUtil.setupStencil(mc.currentFramebuffer, mc.displayWidth, mc.displayHeight)
        drawMapShape(mapRadius, Color.BLACK)

        // activate stencilling
        StencilUtil.enableStencil(StencilUtil.StencilMode.CROP_OUTSIDE)

        // prepare 3D world rendering
        mc.glStateManager.enableTexture2D()
        mc.glStateManager.enableDepth()

        // flip and turn the view to the map overview
        GL11.glScalef(1f, -1f, 1f)
        GL11.glRotatef(perspectiveAngle.toFloat(), 1f, 0f, 0f)
        GL11.glRotatef(mc.thePlayer!!.rotationYaw % 360 - 180, 0f, 1f, 0f)

        // prepare the view entity
        zombie!!.setEntityLocationAndAngles(
                mc.thePlayer!!.posX,
                mc.thePlayer!!.posY + ZOMBY_OFFSET,
                mc.thePlayer!!.posZ,
                mc.thePlayer!!.rotationYaw,
                90f
        )

        // prepare view frustum
        val frustum = factory.newFrustum()
        frustum.setPosition(zombie!!.posX, zombie!!.posY, zombie!!.posZ)

        // setup terrain with new frustum
        mc.renderGlobal.setupTerrain(zombie!!, e.partialTicks, frustum, mc.entityRenderer.frameCount++)

        // bind block textures
        mc.getTextureManagerInstance().bindTexture("textures/atlas/blocks.png")

        // render world layers
        mc.renderGlobal.renderBlockLayer(VibrantRenderGlobal.VibrantBlockLayerType.SOLID, zombie!!)
        mc.renderGlobal.renderBlockLayer(VibrantRenderGlobal.VibrantBlockLayerType.TRANSLUCENT, zombie!!)
        mc.renderGlobal.renderBlockLayer(VibrantRenderGlobal.VibrantBlockLayerType.CUTOUT_MIPPED, zombie!!)
        mc.renderGlobal.renderBlockLayer(VibrantRenderGlobal.VibrantBlockLayerType.CUTOUT, zombie!!)

        // prepare drawing of coordinate system
        mc.glStateManager.disableTexture2D()
        mc.glStateManager.disableDepth()

        // draw coordinate system with base at player
        if (showLines) {
            RenderUtil.drawLine3d(-400.0, -ZOMBY_OFFSET.toDouble(), 0.0, 400.0, -ZOMBY_OFFSET.toDouble(), 0.0, Color.WHITE, lineWidth.toFloat())
            RenderUtil.drawLine3d(0.0, -ZOMBY_OFFSET.toDouble(), -400.0, 0.0, -ZOMBY_OFFSET.toDouble(), 400.0, Color.WHITE, lineWidth.toFloat())
        }
        mc.glStateManager.enableTexture2D()

        // end circular stencil
        StencilUtil.endStencil()

        // reset state
        mc.glStateManager.disableLineSmooth()
        mc.glStateManager.depthMask(true)
        mc.glStateManager.popMatrix()
        mc.glStateManager.popAttrib()
    }

    /**
     * Draw the shape of the map in given color and size. It is assumed, that translation is already at the mid of the shape
     *
     * @param radius radius of the shape
     * @param color color of the shape
     */
    private fun drawMapShape(radius: Int, color: Color) {
        if (mapShape != Shape.SQUARE) {
            val edges = when (mapShape) {
                Shape.CIRCLE -> 360
                Shape.DIAMOND -> 4
                Shape.PENTAGON -> 5
                Shape.HEXAGON -> 6
                Shape.HEPTAGON -> 7
                Shape.OCTAGON -> 8
                else -> -1
            }
            RenderUtil.fillCircleLike(0, 0, radius.toDouble(), edges, color)
        } else {
            RenderUtil.fillRect(-radius, -radius, 2 * radius, 2 * radius, color)
        }
    }
}

enum class Shape {
    CIRCLE, SQUARE, DIAMOND, PENTAGON, HEXAGON, HEPTAGON, OCTAGON
}