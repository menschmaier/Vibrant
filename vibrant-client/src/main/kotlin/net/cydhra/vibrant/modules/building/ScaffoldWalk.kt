package net.cydhra.vibrant.modules.building

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.inventory.MAX_HOTBAR_INVENTORY_SLOT
import net.cydhra.vibrant.api.item.VibrantItemBlock
import net.cydhra.vibrant.api.world.*
import net.cydhra.vibrant.api.world.blocks.VibrantBlockFalling
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.events.render.RenderWorldEvent
import net.cydhra.vibrant.gui.util.RenderUtil
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.settings.by
import net.cydhra.vibrant.settings.clamped
import net.cydhra.vibrant.settings.setting
import net.cydhra.vibrant.util.inventory.InventoryManager
import org.lwjgl.input.Keyboard
import java.awt.Color

class ScaffoldWalk : Module("ScaffoldWalk", DefaultCategories.BUILD, Keyboard.KEY_P) {

    private var replaceBlockPos: VibrantBlockPosition? = null
    private var blockTarget: VibrantBlockPosition? = null
    private var faceTarget: VibrantBlockFacing? = null

    val range by setting(this, "Build Range", 4) {
        clamped(1, 5)
        increment by 1
        decrement by -1
    }

    val preAimDuration by setting(this, "Pre-Aim Ticks", 4) {
        clamped(1, 10)
        increment by 1
        decrement by -1
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer == null)
            return

        // find blocks player will move on
        findTarget().also { (target, face) ->
            blockTarget = target
            faceTarget = face
        }

        // prepare aiming
        // TODO

        // or fire and forget
        if (blockTarget != null) {
            // select building block

            var buildingBlock = -1
            do {
                buildingBlock = InventoryManager.searchItemInHotBar<VibrantItemBlock>(buildingBlock + 1)
            } while (buildingBlock in 0..MAX_HOTBAR_INVENTORY_SLOT &&
                    (mc.thePlayer!!.playerInventory.getStackFromSlot(buildingBlock)?.getItem() as? VibrantItemBlock)?.blockInfo is VibrantBlockFalling)

            if (buildingBlock >= 0) {
                InventoryManager.selectItem(buildingBlock)
            }

            mc.playerController!!.netHandler.sendPacket(factory.newPlayerBlockPlacementPacket(blockTarget!!,
                    faceTarget!!.ordinal,
                    mc.thePlayer!!.playerInventory.currentSelectedItemStack,
                    0.5f,
                    0.5f,
                    0.5f
            )
            )
        }
    }

    @EventHandler
    fun onRender(e: RenderWorldEvent) {
        mc.glStateManager.disableTexture2D()
        mc.glStateManager.enableLineSmooth()

        if (replaceBlockPos != null) {
            RenderUtil.outlineCube(
                    replaceBlockPos!!.posX.toDouble() - mc.getRenderManagerInstance().renderPosX,
                    replaceBlockPos!!.posY.toDouble() - mc.getRenderManagerInstance().renderPosY,
                    replaceBlockPos!!.posZ.toDouble() - mc.getRenderManagerInstance().renderPosZ,
                    replaceBlockPos!!.posX + 1.0 - mc.getRenderManagerInstance().renderPosX,
                    replaceBlockPos!!.posY + 1.0 - mc.getRenderManagerInstance().renderPosY,
                    replaceBlockPos!!.posZ + 1.0 - mc.getRenderManagerInstance().renderPosZ,
                    Color.BLUE,
                    1f
            )
        }

        if (blockTarget != null) {
            RenderUtil.outlineCube(
                    blockTarget!!.posX.toDouble() - mc.getRenderManagerInstance().renderPosX,
                    blockTarget!!.posY.toDouble() - mc.getRenderManagerInstance().renderPosY,
                    blockTarget!!.posZ.toDouble() - mc.getRenderManagerInstance().renderPosZ,
                    blockTarget!!.posX + 1.0 - mc.getRenderManagerInstance().renderPosX,
                    blockTarget!!.posY + 1.0 - mc.getRenderManagerInstance().renderPosY,
                    blockTarget!!.posZ + 1.0 - mc.getRenderManagerInstance().renderPosZ,
                    Color.RED,
                    1f
            )
        }

        mc.glStateManager.enableTexture2D()
        mc.glStateManager.enableLineSmooth()
    }

    /**
     * Find a block position and a facing where the scaffold shall place a block next. It anticipates the player's current movement to get
     * the next block, where they will move on. If this block is mid-air thus it cannot be placed, it will search adjacent blocks to place a
     * helper-block. If no target is found among those possibilities, null is returned.
     *
     * @return a pair of a position and facing where a block should be placed.
     */
    private fun findTarget(): Pair<VibrantBlockPosition?, VibrantBlockFacing?> {
        // find the first block the player will move on that is suitable to be replaced by a building block
        val replaceableBlockPosPrediction: VibrantBlockPosition? =
                (0..preAimDuration)
                        .map { i ->
                            factory.newBlockPosition((mc.thePlayer!!.posX + mc.thePlayer!!.motionX * i).toInt(),
                                    mc.thePlayer!!.posY.toInt() - 1,
                                    (mc.thePlayer!!.posZ + mc.thePlayer!!.motionZ * i).toInt()
                            )
                        }
                        .firstOrNull { pos -> isBlockSuitableForScaffold(mc.theWorld!!.getBlockInfoAt(pos)) }

        // try to find a block that can be used as scaffold for the new block to be placed
        if (replaceableBlockPosPrediction != null) {
            val target = selectTargetFace(replaceableBlockPosPrediction)

            // if found a suitable target for block placement, return it
            if (target != null)
                return target

            // otherwise, search for a block position adjacent to the target block and place that one
            val adjacentBlocks: Array<VibrantBlockPosition> = arrayOf(
                    replaceableBlockPosPrediction.offsetSide(mc.thePlayer!!.horizontalBlockFacing),
                    when (mc.thePlayer!!.horizontalBlockFacing.axis) {
                        VibrantAxis.X -> replaceableBlockPosPrediction.offsetSide(VibrantBlockFacing.SOUTH)
                        else -> replaceableBlockPosPrediction.offsetSide(VibrantBlockFacing.WEST)
                    },
                    when (mc.thePlayer!!.horizontalBlockFacing.axis) {
                        VibrantAxis.X -> replaceableBlockPosPrediction.offsetSide(VibrantBlockFacing.NORTH)
                        else -> replaceableBlockPosPrediction.offsetSide(VibrantBlockFacing.EAST)
                    },
                    replaceableBlockPosPrediction.offsetSide(mc.thePlayer!!.horizontalBlockFacing.opposite())
            )

            // search first best scaffold for the predicted block
            return adjacentBlocks
                    .filter { pos -> isBlockSuitableForScaffold(mc.theWorld!!.getBlockInfoAt(pos)) }
                    .mapNotNull { pos -> selectTargetFace(pos) }
                    .firstOrNull() ?: Pair(null, null)
        }

        // no prediction was made, probably there is already a walkable way in the player's path
        return Pair(null, null)
    }

    /**
     * Select a face, where to place a block against to fill the given block position.
     *
     * @param blockTarget position where a block should be placed
     *
     * @return the position and the facing where to place a block against
     */
    private fun selectTargetFace(blockTarget: VibrantBlockPosition): Pair<VibrantBlockPosition, VibrantBlockFacing>? {
        // check the adjacent block locations of given blockTarget for a suitable block face to place the target block on
        val suitablePlacingTargets: Array<Pair<VibrantBlockPosition, VibrantBlockFacing>> = arrayOf(
                blockTarget.offsetSide(mc.thePlayer!!.horizontalBlockFacing) to mc.thePlayer!!.horizontalBlockFacing.opposite(),
                when (mc.thePlayer!!.horizontalBlockFacing.axis) {
                    VibrantAxis.X -> blockTarget.offsetSide(VibrantBlockFacing.SOUTH) to VibrantBlockFacing.NORTH
                    else -> blockTarget.offsetSide(VibrantBlockFacing.WEST) to VibrantBlockFacing.EAST
                },
                when (mc.thePlayer!!.horizontalBlockFacing.axis) {
                    VibrantAxis.X -> blockTarget.offsetSide(VibrantBlockFacing.NORTH) to VibrantBlockFacing.SOUTH
                    else -> blockTarget.offsetSide(VibrantBlockFacing.EAST) to VibrantBlockFacing.WEST
                },
                blockTarget.offsetSide(mc.thePlayer!!.horizontalBlockFacing.opposite()) to mc.thePlayer!!.horizontalBlockFacing,
                blockTarget.offsetSide(VibrantBlockFacing.DOWN) to VibrantBlockFacing.UP
        )

        // if there's any face where the new block can be placed against, return that
        for ((target, face) in suitablePlacingTargets) {
            if (mc.theWorld!!.getBlockInfoAt(target).normalCube()) {
                return target to face
            }
        }

        // no suitable face for placing found
        return null
    }

    /**
     * @return true, if the block could be used for scaffolding. More specifically, whether a block position independent of the player
     * position can be replaced by a building block. Facings and location are not taken into consideration
     */
    private fun isBlockSuitableForScaffold(info: VibrantBlockInfo): Boolean {
        return info.renderingType == VibrantBlockRenderType.LIQUID || info.renderingType == VibrantBlockRenderType.NO_RENDER
    }
}