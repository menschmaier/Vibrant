package net.cydhra.vibrant.command

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.MissingRequiredPositionalArgumentException
import com.xenomachina.argparser.ShowHelpException
import com.xenomachina.argparser.SystemExitException
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.command.commands.*
import org.cef.callback.CefQueryCallback
import java.io.*

object CommandDispatcher {

    private val commandHandler = mutableMapOf<String, Pair<CommandHandler<Any>, (ArgParser) -> Any>>()

    init {
        registerCommandHandler(UserCommand, { "" }, "usercommand")
        registerCommandHandler(EchoCommand, ::EchoArguments, "echo", "write", "print")
        registerCommandHandler(ToggleCommand, ::ToggleArguments, "toggle", "t")
        registerCommandHandler(ModuleNamesCommand, ::ModuleNamesArguments, "modules", "modulenames")
        registerCommandHandler(ManCommand, ::ManArguments, "man")
        registerCommandHandler(DebugCommand, { "" }, "debug")
        registerCommandHandler(SettingsCommand, ::SettingsCommandArguments, "settings")
        registerCommandHandler(SetCommand, ::SetCommandArguments, "set")
        registerCommandHandler(GetCommand, ::GetCommandArguments, "get")
        registerCommandHandler(ReloadGuiCommand, ::ReloadGuiArguments, "reloadgui")
        registerCommandHandler(LoginCommand, ::LoginCommandArguments, "login")
        registerCommandHandler(SessionCommand, ::SessionArguments, "session")
        registerCommandHandler(OpenFolderCommand, ::OpenFolderArguments, "fopen")
        registerCommandHandler(RenameWorldCommand, ::RenameWorldArguments, "renameworld")
        registerCommandHandler(IntegratedServerCommand, ::IntegratedServerArguments, "integratedserver")
        registerCommandHandler(DeleteWorldCommand, ::DeleteWorldArguments, "deleteworld")
    }

    fun <T : Any> registerCommandHandler(handler: CommandHandler<T>, commandArgumentParser: (ArgParser) -> T, vararg commandAliases: String) {
        logger.debug("registering command alias(es) ${commandAliases.joinToString(", ")}")
        commandAliases
                .forEach { alias ->
                    @Suppress("UNCHECKED_CAST")
                    commandHandler[alias] =
                            Pair<CommandHandler<Any>, (ArgParser) -> Any>(handler as CommandHandler<Any>, commandArgumentParser)
                }
    }

    fun dispatchCommand(command: String, commandSender: CommandSender, callback: CefQueryCallback?) {
        logger.debug("dispatching command \"$command\"")

        if (command == "")
            return

        val arguments = command.split(" ")
        commandHandler[arguments[0]]
                ?.takeIf { commandSender != CommandSender.PLAYER || it.first.userCommand } // ignore client commands from user
                ?.also { (commandHandler, commandArgumentParser) ->
                    if (commandHandler.proxyCommand) { // execute command with the whole argument array as a single string
                        commandHandler.executeCommand(origin = commandSender,
                                label = arguments[0],
                                arguments = arguments.subList(1, arguments.size).joinToString(" "),
                                callback = callback
                        )
                    } else {
                        try { // parse arguments
                            val parser = ArgParser(arguments.subList(1, arguments.size).toTypedArray())
                            val args = commandArgumentParser(parser)
                            parser.force()

                            commandHandler.executeCommand(origin = commandSender,
                                    label = arguments[0],
                                    arguments = args,
                                    callback = callback
                            )
                        } catch (e: ShowHelpException) { // print help on request
                            val baos = ByteArrayOutputStream()
                            val writer = PrintWriter(baos)
                            e.printUserMessage(writer = writer, programName = arguments[0], columns = 200)
                            writer.flush()
                            callback?.success("<pre class=\"manpage\">" + String(baos.toByteArray())
                                    .replace("\n", "<br>") + "</pre>")
                        } catch (e: SystemExitException) {
                            callback?.failure(-1, e.message)
                        }
                    }
                }
                ?: callback?.failure(-1, "${arguments[0]} is not a registered command alias")
                ?: throw IllegalArgumentException("${arguments[0]} is not a registered command alias")
    }

    /**
     * @param commandAlias the command alias which's handler is searched
     *
     * @return the [CommandHandler] associated with the given alias, if any
     */
    fun findCommandHandler(commandAlias: String): CommandHandler<Any>? {
        return commandHandler[commandAlias]?.first
    }
}