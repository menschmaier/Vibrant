package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.gui.GuiQueryHandler
import org.cef.callback.CefQueryCallback

object ReloadGuiCommand : CommandHandler<ReloadGuiArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: ReloadGuiArguments, callback: CefQueryCallback?) {
        GuiQueryHandler.reload()
    }
}

class ReloadGuiArguments(parser: ArgParser) {

}