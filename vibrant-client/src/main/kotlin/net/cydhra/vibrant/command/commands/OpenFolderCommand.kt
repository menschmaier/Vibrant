package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback
import java.awt.Desktop
import java.io.File

/**
 * A command to open directories
 */
object OpenFolderCommand : CommandHandler<OpenFolderArguments>(userCommand = false) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: OpenFolderArguments, callback: CefQueryCallback?) {
        try {
            Desktop.getDesktop().open(arguments.path)
        } catch (e: Throwable) {
            callback?.failure(-1, "Failed to open ${arguments.path.absoluteFile}: ${e.message}")
            return
        }

        callback?.success("Path was opened in the system file manager.")
    }

}

class OpenFolderArguments(parser: ArgParser) {
    val path by parser.positional("PATH",
            help = "path relative to the minecraft home directory",
            transform = {
                File(".", this)
            })
}