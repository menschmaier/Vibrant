package net.cydhra.vibrant.command.commands

import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.settings.VibrantClampedSettingDelegate
import net.cydhra.vibrant.settings.VibrantConfiguration
import net.cydhra.vibrant.util.Keybind
import org.cef.callback.CefQueryCallback
import java.awt.Color

/**
 * Command listing all settings of a module
 */
object SettingsCommand : CommandHandler<SettingsCommandArguments>(userCommand = false) {
    private val gson = GsonBuilder()
            .create()

    override fun executeCommand(origin: CommandSender, label: String, arguments: SettingsCommandArguments, callback: CefQueryCallback?) {
        val delegates = VibrantConfiguration.getModuleSettingDelegates(arguments.module)
        if (delegates.isEmpty()) {
            callback?.failure(-1, "No settings for module \"${arguments.module}\" found.")
            return
        }

        val settingsObj = JsonObject()
        delegates.forEach { delegate ->
            val propertyObject = gson.toJsonTree(when {
                delegate is VibrantClampedSettingDelegate ->
                    RangeSetting(delegate.min as Number, delegate.max as Number, delegate.value as Number)
                delegate.value.javaClass.isEnum ->
                    EnumerationSetting(delegate.value.javaClass.enumConstants, delegate.value)
                delegate.value is Boolean ->
                    BooleanSetting(delegate.value as Boolean)
                delegate.value is Keybind ->
                    KeybindSetting((delegate.value as Keybind).keyCode)
                delegate.value is Color ->
                    ColorSetting(delegate.value as Color)
                else ->
                    throw IllegalArgumentException("Properties of type ${delegate.value.javaClass} are not supported, yet")
            }
            )

            settingsObj.add(delegate.name, propertyObject)
        }

        callback?.success(settingsObj.toString())
    }
}

class SettingsCommandArguments(parser: ArgParser) {
    val module by parser.positional("MODULE", help = "the module of the requested setting(s)")
}

/**
 * Type of a settings property in the data model
 */
enum class SettingType {
    RANGE, ENUM, BOOLEAN, KEYBIND, COLOR
}

/**
 * A settings property
 */
abstract class SettingData<T>(val type: SettingType) {
    abstract val value: T
}

/**
 * A setting with a number in a certain range
 */
data class RangeSetting(val min: Number, val max: Number, override val value: Number) : SettingData<Number>(SettingType.RANGE)

/**
 * A boolean setting with no further constraints
 */
data class BooleanSetting(override val value: Boolean) : SettingData<Boolean>(SettingType.BOOLEAN)

/**
 * A setting for enumeration values. It also takes an array of all enum values
 */
@Suppress("ArrayInDataClass")
data class EnumerationSetting(val values: Array<Any>, override val value: Any) : SettingData<Any>(SettingType.ENUM)

data class KeybindSetting(override val value: Int) : SettingData<Int>(SettingType.KEYBIND)

data class ColorSetting(override val value: Color) : SettingData<Color>(SettingType.COLOR)