package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.settings.VibrantConfiguration
import org.cef.callback.CefQueryCallback

/**
 * A command to retrieve the value of a given setting
 */
object GetCommand : CommandHandler<GetCommandArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: GetCommandArguments, callback: CefQueryCallback?) {
        val delegates = VibrantConfiguration.getModuleSettingDelegates(arguments.module)
        if (delegates.isEmpty()) {
            callback?.failure(-1, "No settings for module \"${arguments.module}\" found.")
            return
        }

        val settingDelegate = delegates.find { it.name.toLowerCase() == arguments.setting.joinToString(" ").toLowerCase() }
        if (settingDelegate == null) {
            callback?.failure(-2, "No setting named \"${arguments.setting}\" in module \"${arguments.module}\".")
            return
        }

        callback?.success("\"${settingDelegate.name}\" is set to ${settingDelegate.value}")
    }

}

class GetCommandArguments(parser: ArgParser) {
    val module by parser.positional("MODULE", help = "the module of the requested setting")
    val setting by parser.positionalList("SETTING", help = "the name of the requested setting")
}