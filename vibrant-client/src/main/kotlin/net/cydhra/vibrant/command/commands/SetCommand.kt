package net.cydhra.vibrant.command.commands

import com.google.gson.Gson
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.settings.VibrantConfiguration
import net.cydhra.vibrant.settings.VibrantSettingDelegate
import org.cef.callback.CefQueryCallback

/**
 * A command for altering module settings
 */
object SetCommand : CommandHandler<SetCommandArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: SetCommandArguments, callback: CefQueryCallback?) {
        val delegates = VibrantConfiguration.getModuleSettingDelegates(arguments.module)
        if (delegates.isEmpty()) {
            callback?.failure(-1, "No settings for module \"${arguments.module}\" found.")
            return
        }

        val settingDelegate = delegates.find { it.name.toLowerCase() == arguments.setting.joinToString(" ").toLowerCase() }
        if (settingDelegate == null) {
            callback?.failure(-2, "No setting named \"${arguments.setting.joinToString(" ")}\" in module \"${arguments.module}\".")
            return
        }

        if (arguments.reset) {
            @Suppress("UNCHECKED_CAST")
            (settingDelegate as VibrantSettingDelegate<Any>).value = settingDelegate.initialValue
        } else {
            try {
                @Suppress("UNCHECKED_CAST")
                (settingDelegate as VibrantSettingDelegate<Any>).value = Gson().fromJson(arguments.value, settingDelegate.value.javaClass)
            } catch (e: ClassCastException) {
                callback?.failure(-3, "${arguments.value} is an invalid value for \"${settingDelegate.name}\"")
                return
            }
        }

        callback?.success("\"${settingDelegate.name}\" was set to ${settingDelegate.value}")
    }
}

class SetCommandArguments(parser: ArgParser) {
    val reset by parser.flagging("-r", "--reset", help = "reset the setting to its default value")
    val value by parser.storing("-v", "--value", help = "the value that shall be stored in the setting").default<String?>(null)

    val module by parser.positional("MODULE", help = "the module of the requested setting")
    val setting by parser.positionalList("SETTING", help = "the name of the requested setting")
}