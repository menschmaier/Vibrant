package net.cydhra.vibrant.api.render

/**
 *
 */
interface VibrantEntityRenderer {
    var frameCount: Int
    var lightmapUpdateNeeded: Boolean

    fun setupOverlayRendering()

    fun disableLightmap()

    fun enableLightmap()

    fun setupCameraTransform(partialTicks: Float)

    fun renderWorld(partialTicks: Float, nanos: Long)

    fun doLightmapUpdate(partialTicks: Float)
}