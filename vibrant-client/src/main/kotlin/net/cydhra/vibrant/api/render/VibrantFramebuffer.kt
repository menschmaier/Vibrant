package net.cydhra.vibrant.api.render

/**
 *
 */
interface VibrantFramebuffer {
    var depthBuffer: Int

    val width: Int
    val height: Int

    val textureWidth: Int
    val textureHeight: Int

    val textureId: Int

    fun deleteFramebuffer()

    fun unbindFramebuffer()

    fun bindFramebuffer(setViewport: Boolean)

    fun bindFramebufferTexture()

    fun unbindFramebufferTexture()
}