package net.cydhra.vibrant.api.render

interface VibrantTessellator {

    val worldRenderer: VibrantVertexBufferBuilder

    fun draw()
}