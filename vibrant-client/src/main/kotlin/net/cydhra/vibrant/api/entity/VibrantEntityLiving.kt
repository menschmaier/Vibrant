package net.cydhra.vibrant.api.entity

import net.cydhra.vibrant.api.util.VibrantDamageSource
import net.cydhra.vibrant.api.util.VibrantVec3

/**
 *
 */
interface VibrantEntityLiving : VibrantEntity {

    fun getLookVector(): VibrantVec3

    fun getDamageSource(): VibrantDamageSource

    fun isClimbing(): Boolean
}