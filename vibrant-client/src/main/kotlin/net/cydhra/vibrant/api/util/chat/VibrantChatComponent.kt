package net.cydhra.vibrant.api.util.chat

interface VibrantChatComponent {
    fun setStyle(style: VibrantChatStyle): VibrantChatComponent

    /**
     * Appends the given component to the end of this one.
     */
    fun appendSiblingComponent(component: VibrantChatComponent): VibrantChatComponent

    fun getComponentSiblings(): List<VibrantChatComponent>

}