package net.cydhra.vibrant.api.client

import com.google.common.util.concurrent.ListenableFuture
import net.cydhra.nidhogg.data.Session
import net.cydhra.vibrant.api.entity.VibrantEntity
import net.cydhra.vibrant.api.entity.VibrantPlayerSP
import net.cydhra.vibrant.api.render.*
import net.cydhra.vibrant.api.world.VibrantWorld
import net.cydhra.vibrant.api.gui.VibrantGuiScreen
import net.cydhra.vibrant.api.world.VibrantWorldSettings
import net.cydhra.vibrant.api.world.storage.VibrantSaveFormat

/**
 *
 */
interface VibrantMinecraft {

    val thePlayer: VibrantPlayerSP?
    val playerController: VibrantPlayerController?

    val theWorld: VibrantWorld?
    val renderGlobal: VibrantRenderGlobal
    val entityRenderer: VibrantEntityRenderer

    var theRenderViewEntity: VibrantEntity

    val currentFramebuffer: VibrantFramebuffer?

    val displayWidth: Int
    val displayHeight: Int

    val isCurrentlyDisplayingScreen: Boolean

    val timer: VibrantTimer

    val gameSettings: VibrantGameSettings
    var minecraftSession: Session

    val glStateManager: VibrantGlStateManager
    val tessellator: VibrantTessellator

    fun getTextureManagerInstance(): VibrantTextureManager

    fun getRenderManagerInstance(): VibrantRenderManager

    fun getTileEntityRenderDispatcherInstance(): VibrantTileEntityRendererDispatcher

    fun displayGuiScreen(screen: VibrantGuiScreen?)

    fun scheduleTask(runnableToSchedule: Runnable): ListenableFuture<Any>

    fun startIntegratedServer(folderName: String, worldName: String, settings: VibrantWorldSettings?)

    fun getSaveLoaderInstance(): VibrantSaveFormat
}