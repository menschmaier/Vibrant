package net.cydhra.vibrant.modulesystem

import com.google.common.reflect.ClassPath
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import net.cydhra.vibrant.settings.VibrantConfiguration
import java.io.File

private const val MODULE_PARENT_PACKAGE = "net.cydhra.vibrant.modules"

/**
 * [ModuleLoader] for modules provided in [MODULE_PARENT_PACKAGE].
 *
 * @see [loadModules]
 */
class LocalModuleLoader : ModuleLoader {

    /**
     * Load modules according to config. All modules from the package [MODULE_PARENT_PACKAGE] and its subpackages are traversed and if the
     * config does not forbid it, they will be registered in the [ModuleManager]
     */
    override fun loadModules(): List<Module> {
        val configFile = File(VibrantConfiguration.SETTINGS_FOLDER, "modules.json")

        val config = if (configFile.exists())
            JsonParser().parse(configFile.reader().readText()).asJsonObject
        else
            JsonObject()

        val moduleList: MutableList<Module> = mutableListOf()

        moduleList += ClassPath
                .from(this.javaClass.classLoader)
                .getTopLevelClassesRecursive(MODULE_PARENT_PACKAGE)
                .map { it.load() }
                .filter { Module::class.java.isAssignableFrom(it) }
                .map { it.newInstance() as Module }
                .filter { module ->
                    if (config.has(module.name)) {
                        return@filter config.get(module.name).asBoolean
                    } else {
                        config.addProperty(module.name, true)
                        return@filter true
                    }
                }
                .toList()

        configFile.also { it.createNewFile() }.writeText(config.toString())
        return moduleList
    }
}