package net.cydhra.vibrant.gui.modules

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.gui.VibrantGuiMainMenu
import net.cydhra.vibrant.events.minecraft.GuiScreenChangeEvent
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.gui.mainmenu.MainMenuGuiManager
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module

class MainMenuModule : Module("MainMenu", DefaultCategories.SYSTEM) {

    private var init = false

    override fun initialize() {
        this.isEnabled = true
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (!init) {
            mc.displayGuiScreen(MainMenuGuiManager.createGuiScreen(factory.newGuiMainMenu()))
            init = true
        }
    }

    @EventHandler
    fun onGuiScreenChange(e: GuiScreenChangeEvent) {
        if ((e.screen == null && mc.theWorld == null) || e.screen is VibrantGuiMainMenu) {
            e.isCancelled = true
            mc.displayGuiScreen(MainMenuGuiManager.createGuiScreen(factory.newGuiMainMenu()))
        }
    }
}