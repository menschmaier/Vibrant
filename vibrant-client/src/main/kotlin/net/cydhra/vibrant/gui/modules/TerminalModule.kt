package net.cydhra.vibrant.gui.modules

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.events.minecraft.KeyboardEvent
import net.cydhra.vibrant.gui.terminal.TerminalGuiManager
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

class TerminalModule : Module("Terminal", DefaultCategories.SYSTEM) {

    override fun initialize() {
        this.isEnabled = true
    }

    @EventHandler
    fun onKeyPressed(e: KeyboardEvent) {
        if (e.type == KeyboardEvent.KeyboardEventType.RELEASE && !mc.isCurrentlyDisplayingScreen) {
            // TODO make configurable
            if (e.keycode == Keyboard.KEY_Y) {
                mc.displayGuiScreen(TerminalGuiManager.createGuiScreen())
            }
        }
    }
}