package net.cydhra.vibrant.gui

import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.api.gui.VibrantGuiController

/**
 * A generic GUI screen that just delegates the controller to the [AbstractVibrantGuiManager] handling this screen.
 */
class GenericVibrantGuiScreen(val guiController: VibrantGuiController = VibrantClient.factory.newGuiController(),
        override val guiModule: AbstractVibrantGuiManager) : AbstractVibrantGuiScreen(guiController) {
    init {
        guiModule.guiController = this.controller
    }
}