package net.cydhra.vibrant.gui

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.gui.clickgui.ClickGuiGuiManager
import net.cydhra.vibrant.gui.mainmenu.MainMenuGuiManager
import net.cydhra.vibrant.gui.terminal.TerminalGuiManager
import org.cef.CefClient
import org.cef.browser.CefBrowser
import org.cef.browser.CefFrame
import org.cef.browser.CefMessageRouter
import org.cef.callback.CefQueryCallback
import org.cef.handler.CefMessageRouterHandlerAdapter

/**
 * Responsible registry for GUI [modules][AbstractVibrantGuiManager].
 * Also, this class handles JS queries from the browser client by calling the previously registered GUI callbacks.
 */
object GuiQueryHandler {

    private val modules = mutableListOf<AbstractVibrantGuiManager>()

    /**
     * Initialize the handler by registering all GUI modules containing the callbacks invoked on a query.
     */
    fun initialize() {
        registerGuiModule(MainMenuGuiManager)
        registerGuiModule(TerminalGuiManager)
        registerGuiModule(ClickGuiGuiManager)
    }

    /**
     * Initialize the Chromium [CefClient] by registering a [MessageRouterHandler] responsible for handling JS queries to Vibrant.
     *
     * @param client [CefClient] that shall receive the [MessageRouterHandler]
     */
    fun initBrowserClient(client: CefClient) {
        val msgRouter = CefMessageRouter.create(CefMessageRouter.CefMessageRouterConfig("vibrantQuery", "vibrantQueryCancel"))
        msgRouter.addHandler(MessageRouterHandler(), true)
        client.addMessageRouter(msgRouter)
    }

    /**
     * Register the given [AbstractVibrantGuiManager] obtaining the methods marked with [QueryMethod]
     *
     * @param module a [AbstractVibrantGuiManager] supposedly implemented as a Companion to a subclass of [AbstractVibrantGuiScreen]
     */
    private fun registerGuiModule(module: AbstractVibrantGuiManager) {
        logger.debug("registering GUI Module ${module.name}...")

        // add to the module list
        modules += module

        // find methods marked as QueryMethod
        module.javaClass.methods.forEach { method ->
            if (method.isAnnotationPresent(QueryMethod::class.java)) {
                val annotation = method.getAnnotation(QueryMethod::class.java)
                logger.debug("registering query method command handler ${annotation.name}")

                // register dummy command handler handling the query method
                CommandDispatcher.registerCommandHandler(object : CommandHandler<NullArguments>(annotation.userCommand) {
                    override fun executeCommand(origin: CommandSender, label: String, arguments: NullArguments, callback: CefQueryCallback?) {
                        try {
                            method.invoke(module, origin, label, callback)
                        } catch (e: IllegalArgumentException) {
                            logger.error("Wrong method descriptor registered as QueryMethod", e)
                        }
                    }
                }, ::NullArguments, annotation.name)
            }
        }
    }

    /**
     * Handles the incoming queries deferring them to the [CommandDispatcher] who in turn will execute the respective command or answer with
     * an error.
     */
    @Suppress("UNUSED_PARAMETER")
    fun handleQuery(browser: CefBrowser, request: String, persistent: Boolean, callback: CefQueryCallback?) {
        CommandDispatcher.dispatchCommand(request, CommandSender.BROWSER, callback)
    }

    /**
     * Reload the browser instances of the modules
     */
    fun reload() {
        modules.forEach(AbstractVibrantGuiManager::scheduleReload)
    }

    /**
     * A [CefMessageRouter] that accepts ingoing queries and dispatches them to [handleQuery]
     */
    class MessageRouterHandler : CefMessageRouterHandlerAdapter() {
        override fun onQuery(browser: CefBrowser, frame: CefFrame?, queryId: Long, request: String, persistent: Boolean, callback: CefQueryCallback?): Boolean {
            GuiQueryHandler.handleQuery(browser, request, persistent, callback)
            return true
        }
    }
}

@Suppress("UNUSED_PARAMETER") // the constructor is required to take one argument of type ArgParser
class NullArguments(parser: ArgParser)