package net.cydhra.vibrant.mixin.util;

import net.cydhra.vibrant.api.util.VibrantBoundingBox;
import net.minecraft.util.AxisAlignedBB;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(AxisAlignedBB.class)
public abstract class AxisAlignedBBMixin implements VibrantBoundingBox {

    @Shadow
    public double minX;

    @Shadow
    public double minY;

    @Shadow
    public double minZ;

    @Shadow
    public double maxX;

    @Shadow
    public double maxY;

    @Shadow
    public double maxZ;

    @Shadow
    public abstract AxisAlignedBB offset(double x, double y, double z);

    @Override
    public double getMaxX() {
        return this.maxX;
    }

    @Override
    public double getMaxY() {
        return this.maxY;
    }

    @Override
    public double getMaxZ() {
        return this.maxZ;
    }

    @Override
    public double getMinX() {
        return this.minX;
    }

    @Override
    public double getMinY() {
        return this.minY;
    }

    @Override
    public double getMinZ() {
        return this.minZ;
    }

    @NotNull
    @Override
    public VibrantBoundingBox offsetBy(double posX, double posY, double posZ) {
        return (VibrantBoundingBox) this.offset(posX, posY, posZ);
    }
}
