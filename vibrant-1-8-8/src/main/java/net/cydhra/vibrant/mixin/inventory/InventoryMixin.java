package net.cydhra.vibrant.mixin.inventory;

import net.cydhra.vibrant.api.entity.VibrantPlayer;
import net.cydhra.vibrant.api.inventory.InventoryIterator;
import net.cydhra.vibrant.api.inventory.VibrantInventory;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.cydhra.vibrant.api.util.VibrantDamageSource;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Arrays;

@Mixin(IInventory.class)
public interface InventoryMixin extends VibrantInventory {

    @Shadow
    public abstract void setInventorySlotContents(int index, ItemStack stack);

    @Shadow
    public abstract boolean isUseableByPlayer(EntityPlayer player);

    @Shadow
    public abstract void openInventory(EntityPlayer player);

    @Shadow
    public abstract void closeInventory(EntityPlayer player);

    @Shadow
    public abstract boolean isItemValidForSlot(int index, ItemStack stack);

    @Shadow
    public abstract int getSizeInventory();

    @Shadow
    ItemStack getStackInSlot(int slot);

    @Shadow
    ItemStack decrStackSize(int index, int count);

    @Override
    default public void setInventorySlotContent(int index, @NotNull VibrantItemStack stack) {
        this.setInventorySlotContents(index, ItemStack.class.cast(stack));
    }

    @Override
    default public boolean canBeUsedByPlayer(@NotNull VibrantPlayer player) {
        return this.isUseableByPlayer((net.minecraft.entity.player.EntityPlayer) player);
    }

    @Override
    default public void openInventoryToPlayer(@NotNull VibrantPlayer player) {
        this.openInventory((net.minecraft.entity.player.EntityPlayer) player);
    }

    @Override
    default public void closeCurrentInventory(@NotNull VibrantPlayer player) {
        this.closeInventory((net.minecraft.entity.player.EntityPlayer) player);
    }

    @Override
    default boolean isItemValidInSlot(int index, @NotNull VibrantItemStack stack) {
        return this.isItemValidForSlot(index, ItemStack.class.cast(stack));
    }

    @NotNull
    @Override
    default public InventoryIterator iterator() {
        return new InventoryIterator(this);
    }

    @SuppressWarnings("SuspiciousToArrayCall")
    @Override
    default public int getEnchantmentModifier(@NotNull VibrantItemStack[] armor,
            @NotNull VibrantDamageSource source) {
        return EnchantmentHelper.getEnchantmentModifierDamage(Arrays.asList(armor).toArray(new ItemStack[armor.length]), (DamageSource) source);
    }

    @Override
    default int getInventorySize() {
        return this.getSizeInventory();
    }

    @Nullable
    @Override
    default VibrantItemStack getStackFromSlot(int index) {
        return VibrantItemStack.class.cast(this.getStackInSlot(index));
    }

    @Nullable
    @Override
    default VibrantItemStack decreaseStackSize(int index, int count) {
        return VibrantItemStack.class.cast(this.decrStackSize(index, count));
    }


}
