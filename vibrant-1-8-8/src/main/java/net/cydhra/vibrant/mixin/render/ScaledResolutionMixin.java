package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantScaledResolution;
import net.minecraft.client.gui.ScaledResolution;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(ScaledResolution.class)
public abstract class ScaledResolutionMixin implements VibrantScaledResolution {

}
