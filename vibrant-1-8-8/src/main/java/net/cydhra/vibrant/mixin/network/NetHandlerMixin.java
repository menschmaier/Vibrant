package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantNetHandler;
import net.cydhra.vibrant.api.network.VibrantPacket;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(NetHandlerPlayClient.class)
public abstract class NetHandlerMixin implements VibrantNetHandler {

    @Shadow
    private NetworkManager netManager;

    @Override
    public void sendPacket(VibrantPacket packet) {
        this.netManager.sendPacket((net.minecraft.network.Packet) packet);
    }

}
