package net.cydhra.vibrant.mixin.world;

import net.cydhra.vibrant.api.world.VibrantBlockInfo;
import net.cydhra.vibrant.api.world.VibrantBlockRenderType;
import net.minecraft.block.Block;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Block.class)
public abstract class BlockMixin implements VibrantBlockInfo {

    @Shadow
    public abstract int getRenderType();

    @Shadow
    public abstract boolean isNormalCube();

    @Shadow
    public abstract boolean isTranslucent();

    @Shadow
    public abstract int getLightOpacity();

    @Override
    public VibrantBlockRenderType getRenderingType() {
        return VibrantBlockRenderType.byRenderId(this.getRenderType());
    }

    @Override
    public boolean normalCube() {
        return this.isNormalCube();
    }

    @Override
    public boolean translucent() {
        return this.isTranslucent();
    }

    @Override
    public int getLightOpacityValue() {
        return this.getLightOpacity();
    }


}
