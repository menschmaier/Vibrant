package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantPlayerPacket;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(C03PacketPlayer.class)
public abstract class PacketPlayerMixin implements VibrantPlayerPacket {

    @Shadow
    private boolean onGround;

    @Override
    public boolean getOnGround() { return this.onGround; }

    @Override
    public void setOnGround(boolean onGround) { this.onGround = onGround; }
}
