package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.cydhra.vibrant.api.network.ClickType;
import net.cydhra.vibrant.api.network.UsedMouseButton;
import net.cydhra.vibrant.api.network.VibrantWindowClickPacket;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C0EPacketClickWindow;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(C0EPacketClickWindow.class)
public abstract class ClickWindowPacketMixin implements VibrantWindowClickPacket {

    @Shadow
    private int windowId;

    @Shadow
    private int slotId;

    @Shadow
    private int usedButton;

    @Shadow
    private short actionNumber;

    @Shadow
    private ItemStack clickedItem;

    @Shadow
    private int mode;

    @Override
    public void setWindowGuiId(int id) { this.windowId = id; }

    @Override
    public void setInventorySlotId(int id) { this.slotId = id; }

    @NotNull
    @Override
    public UsedMouseButton getMouseButton() {
        switch (this.usedButton) {
            case 0:
                return UsedMouseButton.LEFT;
            case 1:
                return UsedMouseButton.RIGHT;
            case 2:
                return UsedMouseButton.MIDDLE;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void setMouseButton(@NotNull UsedMouseButton mouseButton) {
        this.usedButton = mouseButton.ordinal();
    }

    @Override
    public void setClickActionNumber(short number) {
        this.actionNumber = number;
    }

    @Override
    public void setClickedItemStack(VibrantItemStack stack) {
        this.clickedItem = ItemStack.class.cast(stack);
    }

    @NotNull
    @Override
    public ClickType getClickType() {
        return ClickType.values()[this.mode];
    }

    @Override
    public void setClickType(@NotNull ClickType type) {
        this.mode = type.ordinal();
    }

    @Override
    public int getWindowGuiId() {
        return this.windowId;
    }

    @Override
    public int getInventorySlotId() {
        return this.slotId;
    }

    @Override
    public short getClickActionNumber() {
        return this.actionNumber;
    }

    @Nullable
    @Override
    public VibrantItemStack getClickedItemStack() {
        return VibrantItemStack.class.cast(this.clickedItem);
    }
}
