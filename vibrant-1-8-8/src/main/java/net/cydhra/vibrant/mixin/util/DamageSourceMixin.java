package net.cydhra.vibrant.mixin.util;

import net.cydhra.vibrant.api.util.VibrantDamageSource;
import net.minecraft.util.DamageSource;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(DamageSource.class)
public abstract class DamageSourceMixin implements VibrantDamageSource {
}
