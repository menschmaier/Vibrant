package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantTextureManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(TextureManager.class)
public abstract class TextureManagerMixin implements VibrantTextureManager {

    @Shadow
    public abstract void bindTexture(ResourceLocation location);

    @Override
    public void bindTexture(@NotNull String location) {
        this.bindTexture(new ResourceLocation(location));
    }
}
