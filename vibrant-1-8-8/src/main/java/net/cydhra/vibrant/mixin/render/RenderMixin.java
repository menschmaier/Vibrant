package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.render.VibrantRender;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Render.class)
public abstract class RenderMixin<T extends Entity> implements VibrantRender {

    @Shadow
    public abstract void doRender(T entity, double x, double y, double z, float entityYaw, float partialTicks);

    @Override
    public void render(@NotNull VibrantEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
        this.doRender((T) entity, x, y, z, entityYaw, partialTicks);
    }
}
