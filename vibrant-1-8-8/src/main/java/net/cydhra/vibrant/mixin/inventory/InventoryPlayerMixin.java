package net.cydhra.vibrant.mixin.inventory;

import net.cydhra.vibrant.api.inventory.VibrantPlayerInventory;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Arrays;

@Mixin(InventoryPlayer.class)
public abstract class InventoryPlayerMixin implements VibrantPlayerInventory {

    @Shadow
    private ItemStack[] armorInventory;

    @Shadow
    public abstract ItemStack getCurrentItem();

    @SuppressWarnings("SuspiciousToArrayCall")
    @NotNull
    @Override
    public VibrantItemStack[] getArmorInventory() {
        return Arrays.asList(this.armorInventory).toArray(new VibrantItemStack[this.armorInventory.length]);
    }

    @Override
    public VibrantItemStack getCurrentSelectedItemStack() {
        return VibrantItemStack.class.cast(this.getCurrentItem());
    }
}
